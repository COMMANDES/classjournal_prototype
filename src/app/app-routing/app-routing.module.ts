import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageNotFoundComponent } from '../page-not-found/page-not-found.component';
import { JournalModule } from '../journal/journal.module';
import { JournalComponent } from '../journal/journal.component';
import { ScheduleComponent } from '../journal/schedule/schedule.component';
import { AdminComponent } from '../journal/admin/admin.component';
import { TeacherComponent } from '../journal/teacher/teacher.component';
import { LearnerComponent } from '../journal/learner/learner.component';
import { AuthComponent } from '../journal/auth/auth.component';

const routes: Routes = [
  { path: '', component: JournalComponent },
  { path: 'schedule', component: ScheduleComponent },
  { path: 'auth', component: AuthComponent },
  { path: 'learner', component: LearnerComponent },
  { path: 'learner/:id', component: LearnerComponent },
  { path: 'teacher/:id', component: TeacherComponent },
  { path: 'admin', component: AdminComponent },
  { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  imports: [JournalModule, RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
