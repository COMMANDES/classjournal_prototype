import { Lesson } from '../journal/shared/models/lesson.model';
import { Course } from '../journal/shared/models/course.model';
import { Group } from '../journal/shared/models/group.model';
import { Learner } from '../journal/shared/models/learner.model';
import { Teacher } from '../journal/shared/models/teacher.model';
import { Grade } from '../journal/shared/models/grade.model';

export const fix_lessons: Lesson[] = [
    { id: 0, topic: 'Первый урок', date: '1.09.2014', homework: '#МамаНамНичегоНеЗадали' },
    { id: 1, topic: 'Второй урок', date: '2.09.2014', homework: 'jwyegrtw' },
    { id: 2, topic: 'Третий урок', date: '3.09.2014', homework: '5vb6n7ib' },
    { id: 3, topic: 'Четвёртый урок', date: '4.09.2014', homework: 'vbtrebyv' },
    { id: 4, topic: 'Пятый урок', date: '1.10.2014', homework: 'vebtvzscdfghf' },
    { id: 5, topic: 'Шестой урок', date: '4.11.2014', homework: 'sdgfhjjkflyut' },
];

export const fix_courses: Course[] = [
    { id: 0, name: 'Мемология', lessons: [0, 1, 2, 3, 4] },
    { id: 1, name: 'Рофловедение', lessons: [2, 3, 4, 5] },
    { id: 2, name: 'Прикладной троллинг', lessons: [0, 2, 4, 5] },
    { id: 3, name: 'jkhfgdhbg', lessons: [1, 3, 4] },
    { id: 4, name: 'etyrhfwjey', lessons: [0, 3, 5] },
    { id: 5, name: 'bwyr', lessons: [1, 2, 4] },
    { id: 6, name: '4g56h7', lessons: [0, 2, 3] },
];

export const fix_groups: Group[] = [
    { id: 0, name: '1A', courses: [0, 1, 5] },
    { id: 1, name: '3B', courses: [1, 2, 4] },
    { id: 2, name: '2Д', courses: [0, 3, 4, 5] },
    { id: 3, name: '4Г', courses: [4, 1] },
];

export const fix_learners: Learner[] = [
    { id: 0, firstname: 'имя0', lastname: 'фамилия0', middlename: 'отчество0', group: 2 },
    { id: 1, firstname: 'имя1', lastname: 'фамилия1', middlename: 'отчество1', group: 1 },
    { id: 2, firstname: 'имя2', lastname: 'фамилия2', middlename: 'отчество2', group: 3 },
    { id: 3, firstname: 'имя3', lastname: 'фамилия3', middlename: 'отчество3', group: 2 },
    { id: 4, firstname: 'имя4', lastname: 'фамилия4', middlename: 'отчество4', group: 1 },
    { id: 5, firstname: 'имя5', lastname: 'фамилия5', middlename: 'отчество5', group: 0 },
    { id: 6, firstname: 'имя6', lastname: 'фамилия6', middlename: 'отчество6', group: 3 },
];

export const fix_teachers: Teacher[] = [
    { id: 0, firstname: 'имя0', lastname: 'фамилия0', middlename: 'отчество0', groups: [0, 3] },
    { id: 1, firstname: 'имя1', lastname: 'фамилия1', middlename: 'отчество1', groups: [1, 2, 4] },
    { id: 2, firstname: 'имя2', lastname: 'фамилия2', middlename: 'отчество2', groups: [0, 2, 3, 5] },
    { id: 3, firstname: 'имя3', lastname: 'фамилия3', middlename: 'отчество3', groups: [0, 1, 2, 3, 4, 5, 6] },
];

export const fix_grades: Grade[] = [
    {learner_id: 0, lesson_id: 0, grade: ''},
    {learner_id: 0, lesson_id: 1, grade: '5'},
    {learner_id: 0, lesson_id: 2, grade: '3'},
    {learner_id: 1, lesson_id: 0, grade: 'Н'},
    {learner_id: 2, lesson_id: 0, grade: '1'},
    {learner_id: 3, lesson_id: 0, grade: '2'},
    {learner_id: 4, lesson_id: 1, grade: '3'},
    {learner_id: 5, lesson_id: 1, grade: '5'},
    {learner_id: 6, lesson_id: 1, grade: '4'},
    {learner_id: 5, lesson_id: 1, grade: '5'},
    {learner_id: 2, lesson_id: 2, grade: '4'},
    {learner_id: 4, lesson_id: 2, grade: 'Н'},
];

