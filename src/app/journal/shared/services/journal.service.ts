import { Injectable } from '@angular/core';
import { Course } from '../models/course.model';
import { Group } from '../models/group.model';
import { Learner } from '../models/learner.model';
import { Lesson } from '../models/lesson.model';
import { Teacher } from '../models/teacher.model';
import { Grade } from '../models/grade.model';
import { fix_lessons, fix_courses, fix_groups, fix_teachers, fix_learners, fix_grades } from 'src/app/_helpers/fixtures';
import { of } from 'rxjs';

const LOCAL_STORAGE_KEYS: string[] = ['lessons', 'courses',
  'groups', 'teachers', 'learners', 'grades'];

@Injectable({
  providedIn: 'root'
})
export class JournalService {
  private journal;
  courses: Course[];
  groups: Group[];
  learners: Learner[];
  lessons: Lesson[];
  teachers: Teacher[];
  grades: Grade[];

  fixtures = [fix_lessons, fix_courses, fix_groups, fix_teachers, fix_learners, fix_grades];

  constructor() {
    for (let i = 0; i < LOCAL_STORAGE_KEYS.length; i++) {
      this[LOCAL_STORAGE_KEYS[i]] = JSON.parse(localStorage.getItem(LOCAL_STORAGE_KEYS[i]));
      // console.log(this[LOCAL_STORAGE_KEYS[i]]);
    }
  }
  loadFixtures() {
    for (let i = 0; i < LOCAL_STORAGE_KEYS.length; i++) {
      localStorage.setItem(LOCAL_STORAGE_KEYS[i], JSON.stringify(this.fixtures[i]));
    }
  }

  update() {
    for (let i = 0; i < LOCAL_STORAGE_KEYS.length; i++) {
      this[LOCAL_STORAGE_KEYS[i]] = JSON.parse(localStorage.getItem(LOCAL_STORAGE_KEYS[i]));
    }
  }
  getLearnersJournal() {
    let returnable_data = [{}];
    returnable_data = this.learners;
    for (let i = 0; i < this.groups.length; i++) {
      for (let j = 0; j < returnable_data.length; j++) {
        if (this.groups[i].hasOwnProperty('id') && this.groups[i]['id'] === returnable_data[j]['group']) {
          returnable_data[j]['group_name'] = this.groups[i]['name'];
          returnable_data[j]['group_courses'] = this.groups[i]['courses'];
        }
      }
    }
    return returnable_data;
  }
  getTeachersJournal() {
    let returnable_data = [{}];
    returnable_data = this.teachers;
    for (let i = 0; i < this.groups.length; i++) {
      for (let j = 0; j < returnable_data.length; j++) {
        if (returnable_data[j]['group_names'] === undefined) { returnable_data[j]['group_names'] = []; }
        if (returnable_data[j]['group_courses'] === undefined) { returnable_data[j]['group_courses'] = []; }
        for (let k = 0; k < returnable_data[j]['groups'].length; k++) {
          if (this.groups[i].hasOwnProperty('id') && this.groups[i].id === returnable_data[j]['groups'][k]) {
            returnable_data[j]['group_names'][k] = this.groups[i].name;
            returnable_data[j]['group_courses'][k] = this.groups[i].courses;
          }
        }
      }
    }
    return returnable_data;
  }
  deleteLearner(id: number) {
    for (let i = 0; i < this.learners.length; i++) {
      if (this.learners[i].hasOwnProperty('id') && this.learners[i].id === id) { this.learners.splice(i, 1); }
    }
    for (let i = 0; i < this.grades.length; i++) {
      // console.log(this.grades[i].learner_id); console.log(id);
      if (this.grades[i].hasOwnProperty('learner_id') && this.grades[i].learner_id === id) { this.grades.splice(i, 1); }
    }
    localStorage.setItem(LOCAL_STORAGE_KEYS[4], JSON.stringify(this.learners));
    localStorage.setItem(LOCAL_STORAGE_KEYS[5], JSON.stringify(this.grades));
  }
  deleteTeacher(id: number) {
    for (let i = 0; i < this.teachers.length; i++) {
      if (this.teachers[i].hasOwnProperty('id') && this.teachers[i].id === id) { this.teachers.splice(i, 1); }
    }
    localStorage.setItem(LOCAL_STORAGE_KEYS[4], JSON.stringify(this.teachers));
  }
  getLessons(learner_id?: number, ...id) {
    const lessons = [];
    if (learner_id && learner_id !== undefined || learner_id !== null) {
      let returnable_data;
      returnable_data = [];
      returnable_data.courses = [];
      for (let i = 0; i < this.groups.length; i++) {
        if (this.groups[i].id === this.learners[learner_id].group) {
          for (let j = 0; j < this.groups[i].courses.length; j++) {
            returnable_data.courses.push(this.courses[j]);
          }
        }
      }
      for (let k = 0; k < returnable_data.courses.length; k++) {
        for (let i = 0; i < returnable_data.courses[k].lessons.length; i++) {
          for (let j = 0; j < this.lessons.length; j++) {
            if (returnable_data.courses[k].lessons[i] === this.lessons[j].id) {
              returnable_data.courses[k].lessons[i] = this.lessons[j];
            }
          }
        }
      }
      console.log(returnable_data);
      return of (returnable_data.length < 1 ? returnable_data : undefined);
    } else {
      if (id[0].length === 0) {
        return of (this.lessons);
      } else {
        for (let i = 0; i < id.length; i++) {
          lessons.push(this.lessons.find((item: Lesson) => item.id === id[i]));
        }
        return of (lessons);
      }
    }
  }
  getLearner(id: number) {
    // id -= 0;
    const returnable_data = [];
    for (let i = 0; i < this.learners.length; i++) {
      // console.log(i); console.log(this.learners.length);
      // console.log(id);
      // console.log(this.learners[i].id);
      // console.log(this.learners[i].id === id);
      if (this.learners[i].hasOwnProperty('id') && this.learners[i].id === id) {
        // console.log(returnable_data);
        // console.log(1);
        returnable_data[0] = this.learners[i];
        break;
      }
    }
    returnable_data[0]['grades'] = [];
    for (let i = 0; i < this.grades.length; i++) {
      if (this.grades[i].hasOwnProperty('learner_id') && this.grades[i].learner_id === id) {
        returnable_data[0]['grades'].push(this.grades[i]);
      }
    }
    // console.log(returnable_data);
    return returnable_data;
  }
  getGrades(learner_id: number) {
    let returnable_data;
    returnable_data = [];
    for (let i = 0; i < this.grades.length; i++) {
      if (this.grades[i].hasOwnProperty('learner_id') && this.grades[i].learner_id === learner_id) {
        returnable_data.push(this.grades[i]);
      }
    }
    // console.log(returnable_data);
    return returnable_data;
  }
  updateGrade(lesson_id: number, learner_id: number, grade) {
    for (let i = 0; i < this.grades.length; i++) {
      if (this.grades[i].hasOwnProperty('learner_id') && this.grades[i].learner_id === learner_id) {
        if (this.grades[i].hasOwnProperty('lesson_id') && this.grades[i].lesson_id === lesson_id) {
        this.grades[i].grade = grade;
        localStorage.setItem(LOCAL_STORAGE_KEYS[5], JSON.stringify(this.grades));
        break;
        }
      }
    }
  }
}
