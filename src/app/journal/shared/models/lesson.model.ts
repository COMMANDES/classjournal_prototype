export class Lesson {
  constructor (id: number, topic: string, date: string, homework: string) {
    this.id = id;
    this.topic = topic;
    this.date = date;
    this.homework = homework;
  }
  id: number;
  topic: string;
  date: string;
  homework: string;
}
