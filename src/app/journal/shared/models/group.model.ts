import { Course } from './course.model';

export class Group {
  constructor(id: number, name: string, courses: Course['id'][]) {
    this.id = id;
    this.name = name;
    this.courses = courses;
  }
  id: number;
  name: string;
  courses: Course['id'][];
}
