import { Group } from './group.model';

export class Learner {
  constructor(id: number, firstname: string, lastname: string, middlename: string, group: Group['id']) {
    this.id = id;
    this.firstname = firstname;
    this.lastname = lastname;
    this.middlename = middlename;
    this.group = group;
  }
  id: number;
  firstname: string;
  lastname: string;
  middlename: string;
  group: Group['id'];
}
