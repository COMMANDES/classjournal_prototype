import { Lesson } from './lesson.model';

export class Course {
  constructor(id: number, name: string, lessons: Lesson['id'][]) {
    this.id = id;
    this.name = name;
    this.lessons = lessons;
  }
  id: number;
  name: string;
  lessons: Lesson['id'][];
}
