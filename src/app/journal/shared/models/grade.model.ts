export class Grade {
  constructor(learner_id: number, lesson_id: number, grade: '' | 'Н' | '1' | '2' | '3' | '4' | '5') {
    this.learner_id = learner_id;
    this.lesson_id = lesson_id;
    this.grade = grade;
  }
  learner_id: number;
  lesson_id: number;
  grade: '' | 'Н' | '1' | '2' | '3' | '4' | '5';
}
