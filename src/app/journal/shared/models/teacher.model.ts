import { Group } from './group.model';

export class Teacher {
  constructor(id: number, firstname: string, lastname: string, middlename: string, groups: Group['id'][]) {
    this.id = id;
    this.firstname = firstname;
    this.lastname = lastname;
    this.middlename = middlename;
    this.groups = groups;
  }
  id: number;
  firstname: string;
  lastname: string;
  middlename: string;
  groups: Group['id'][];
}
