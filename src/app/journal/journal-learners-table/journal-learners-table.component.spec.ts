import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JournalLearnersTableComponent } from './journal-learners-table.component';

describe('JournalLearnersTableComponent', () => {
  let component: JournalLearnersTableComponent;
  let fixture: ComponentFixture<JournalLearnersTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JournalLearnersTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JournalLearnersTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
