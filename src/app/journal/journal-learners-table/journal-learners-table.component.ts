import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog } from '@angular/material';
import { JournalService } from '../shared/services/journal.service';
import { LearnerComponent } from '../learner/learner.component';

@Component({
  selector: 'app-journal-learners-table',
  templateUrl: './journal-learners-table.component.html',
  styleUrls: ['./journal-learners-table.component.scss']
})
export class JournalLearnersTableComponent implements OnInit {
  displayedColumnsLearners: string[] = ['id', 'firstname', 'lastname', 'middlename', 'group_name', 'group_courses'];
  dataSourceLearners = new MatTableDataSource(this.journalService.getLearnersJournal());
  @ViewChild(MatPaginator) paginatorLearners: MatPaginator;
  @ViewChild(MatSort) sortLearners: MatSort;
  constructor(private journalService: JournalService, public dialog: MatDialog) { }

  ngOnInit() {
    this.dataSourceLearners.paginator = this.paginatorLearners;
    this.dataSourceLearners.sort = this.sortLearners;
  }
  alert(row) {
    alert(row.id + ' ' + row.lastname + ' ' + row.firstname + ' ' + row.middlename + ' ' + row.group_name);
  }
  opendialog() {
    const dialogRef = this.dialog.open(LearnerComponent, {
      data: { name: 'austin' },
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

}
