import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JournalComponent } from './journal.component';
import { ScheduleComponent } from './schedule/schedule.component';
import { MatButtonModule, MatListModule, MatGridListModule, MatCardModule, MatDatepickerModule,
  MatNativeDateModule, MatFormFieldModule, MatInputModule, MatSelectModule, MatDividerModule,
  MatTabsModule, MatDialogModule, MatTableModule, MatPaginatorModule, MatSortModule, MatMenuModule,
  MatIconModule } from '@angular/material';
import { AdminModule } from './admin/admin.module';
import { TeacherComponent } from './teacher/teacher.component';
import { LearnerComponent } from './learner/learner.component';
import { AuthModule } from './auth/auth.module';
import { JournalLearnersTableComponent } from './journal-learners-table/journal-learners-table.component';
import { JournalTeachersTableComponent } from './journal-teachers-table/journal-teachers-table.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    AdminModule,
    AuthModule,
    MatButtonModule,
    MatListModule,
    MatCardModule,
    MatDialogModule,
    MatMenuModule,
    MatIconModule,
    MatGridListModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatDividerModule,
    MatTabsModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatDialogModule,
    MatGridListModule,
  ],
  declarations: [JournalComponent, ScheduleComponent, TeacherComponent, LearnerComponent,
    JournalLearnersTableComponent, JournalTeachersTableComponent]
})
export class JournalModule { }
