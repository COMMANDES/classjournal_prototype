import { Component, OnInit, Inject } from '@angular/core';
import { Lesson } from '../shared/models/lesson.model';
import { Course } from '../shared/models/course.model';
import { Group } from '../shared/models/group.model';
import { Teacher } from '../shared/models/teacher.model';
import { Learner } from '../shared/models/learner.model';
import { Grade } from '../shared/models/grade.model';
import { JournalService } from '../shared/services/journal.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  journalservise: JournalService = new JournalService();
  add_any = { LOCAL_STORAGE_KEY: '' };

  lessons = [];
  _lessons: Lesson[]; _courses: Course[]; _groups: Group[]; _teachers: Teacher[]; _learners: Learner[]; _grades: Grade[];
  journal = { lessons: [], courses: [], groups: [], teachers: [], learners: [], grades: [] };
  lesson: Lesson = { id: 0, topic: '', date: '', homework: '' };
  course: Course = { id: 0, name: 'Мемология', lessons: [0, 1, 2, 3, 4]};
  group: Group = null;
  teacher: Teacher = null;
  learner: Learner = null;
  grade: Grade = { learner_id: 0, lesson_id: 0, grade: '' };
  grade_values = ['', 'Н', '1', '2', '3', '4', '5'];
  card_title = ['Урок', 'Предмет', 'Класс', 'Учитель', 'Ученик', 'Оценка'];
  // __lessons = new FormControl();
  constructor() {
    this._lessons = this.journalservise.lessons;
  }
  // getid(subarray_name: 'lesson' | 'course' | 'group' | 'teacher' | 'learner' | 'grade') {
  //   this.journal = JSON.parse(localStorage.getItem(LOCAL_STORAGE_KEY)) || [];
  //   if (this[subarray_name + 's'] !== undefined) {
  //     return (this[subarray_name + 's'].length || 0); } else { return 0; }
  // }
  // updateId(...subarray_names) {
  //   this.journal = JSON.parse(localStorage.getItem(LOCAL_STORAGE_KEY)) || [];
  //   for (let i = 0; i < subarray_names.length; i++) {
  //     if (this[subarray_names[i] + 's'] !== undefined && this[subarray_names[i] + 's'] !== undefined) {
  //       this[subarray_names[i] + 's'].id = this[subarray_names[i] + 's'].length || 0;
  //     }
  //   }
  // }
  ngOnInit() {
    this.journalservise.update();
    // this.updateId('lesson', 'course');
    // console.log(this._lessons);
    // this.getobjects('lessons', 'lessons', 'courses', 'groups', 'teachers', 'learners', 'grades');
    // console.log(this._lessons);
    // this.initJournal();
  }

  // addItem(item_name: 'lesson' | 'course' | 'group' | 'teacher' | 'learner' | 'grade') {
  //   console.log(this.course.lessons);
  //   // console.log(this.journal);
  //   // console.log(this[item_name]);
  //   // console.log(this['_' + item_name + 's']);
  //   this['_' + item_name + 's'] = this.journal[item_name + 's'] || [];
  //   this['_' + item_name + 's'].push(JSON.stringify(this[item_name]));
  //   // console.log(this['_' + item_name + 's']);
  //   this.writeChanges();
  // }
  addanyfix() {
    // console.log(this.lessons); console.log(this.lesson);
    this.lessons.push(this.lesson);
    localStorage.setItem(this.add_any.LOCAL_STORAGE_KEY, JSON.stringify(this.lessons));
  }
  getobjects(...subarray_names) {
    // console.log(subarray_names);
    return this.journalservise.getLessons(null, subarray_names);
  }
  // initJournal() {
  //   const subarray_names = ['lessons', 'courses', 'groups', 'teachers', 'learners', 'grades'];
  //   try {
  //     for (let i = 0; i < subarray_names.length; i++) {
  //       this[i] = this.getobjects(subarray_names[i]);
  //     }
  //   } catch (error) {
  //     console.log(error);
  //   }
  // }
  // writeChanges() {
  //   // console.log(this['_lessons']);
  //   // console.log(this.journal.lessons);
  //   this.journal.lessons = this._lessons;
  //   // console.log(this.journal.lessons);
  //   this.journal.courses = this._courses;
  //   this.journal.groups = this._groups;
  //   this.journal.teachers = this._teachers;
  //   this.journal.learners = this._learners;
  //   this.journal.grades = this._grades;
  //   // console.log(this.journal);
  //   // console.log(JSON.stringify(this.journal));
  //   localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(Object.assign({}, this.journal)));
  //   this.updateId('lesson', 'course');
  // }
  setvalue(_var, _val): void {
    console.log(_val);
    _var.push(_val);
  }
  loadFixtures() {
    this.journalservise.loadFixtures();
  }
  _alert() {
    alert('alert!');
  }
  deleteLearner(id: number) {
    this.journalservise.deleteLearner(4);
  }
  log_date(event) {
    console.log(event.value);
  }
}
