import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin.component';
import { MatButtonModule, MatCardModule, MatFormFieldModule, MatSelectModule, MatInputModule,
  MatTabsModule, MatDividerModule, MatDialogModule, MatGridListModule, MatListModule,
  MatDatepickerModule, MatNativeDateModule } from '@angular/material';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MatListModule,
    MatButtonModule,
    MatCardModule,
    MatGridListModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatDividerModule,
    MatTabsModule,
    MatDialogModule,
    MatGridListModule,
  ],
  declarations: [AdminComponent]
})
export class AdminModule { }
