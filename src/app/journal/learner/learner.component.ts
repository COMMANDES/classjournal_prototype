import { Component, OnInit, ViewChild } from '@angular/core';
import { JournalService } from '../shared/services/journal.service';
import { MatMenuTrigger } from '@angular/material';
import { Observable, BehaviorSubject } from 'rxjs';
import { Grade } from '../shared/models/grade.model';

@Component({
  selector: 'app-learner',
  templateUrl: './learner.component.html',
  styleUrls: ['./learner.component.scss']
})
export class LearnerComponent implements OnInit {
  userid: number;
  response_data: any;
  learner_courses_and_lessons;
  grade_values = ['', 'Н', '1', '2', '3', '4', '5'];
  // learner_courses_and_lessons: BehaviorSubject<Array<any>> = new BehaviorSubject<Array<any>>(null);
  constructor(private journalService: JournalService) { }
  @ViewChild(MatMenuTrigger)
  _click_gradeMenu: MatMenuTrigger;
  _click_gradeMenuPosition = {x: '0px', y: '0px'};
  learner: {};
  learner_id: number;
  ngOnInit() {
    this.learner_id = 0;
    this.getLearner(this.learner_id);
    console.log(this.learner);
    // this.learner_courses_and_lessons = this.journalService.getLessons(0);
    //  console.log(this.learner_courses_and_lessons);
    this.journalService.getLessons(this.learner_id).subscribe((response_data) => {this.learner_courses_and_lessons = response_data;
      console.log(this.learner_courses_and_lessons); /* console.log(response_data); */});
  }
  getLearner(id: number) {
    this.learner = this.journalService.getLearner(id);
  }
  log_data(event) {
    // console.log(event.value);s
  }
  getGrades(learner_id: number) {
    const temp = this.journalService.getGrades(learner_id);
    // console.log(temp);
    return temp;
  }
  trackGrades(index, item) {
    return item ? item.grade : undefined;
  }
  _click_grade(event: MouseEvent, item) {
    // console.log(item);
    event.preventDefault();
    this._click_gradeMenuPosition.x = event.clientX + 'px';
    this._click_gradeMenuPosition.y = event.clientY + 'px';
    this._click_gradeMenu.menuData = {'item': item};
    this._click_gradeMenu.openMenu();
  }
  updateGrade(lesson_id: number, learner_id: number, grade) {
    // alert(lesson_id + ' ' + learner_id + ' ' + grade);
    this.journalService.updateGrade(lesson_id, learner_id, grade);
  }
  getLessons(learner_id: number) {
    this.journalService.getLessons(learner_id).subscribe((response_data) => {this.learner_courses_and_lessons = response_data;
    console.log(this.learner_courses_and_lessons); });
  }
}
