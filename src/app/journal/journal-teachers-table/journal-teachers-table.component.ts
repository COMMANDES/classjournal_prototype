import { Component, OnInit, ViewChild } from '@angular/core';
import { JournalService } from '../shared/services/journal.service';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';

@Component({
  selector: 'app-journal-teachers-table',
  templateUrl: './journal-teachers-table.component.html',
  styleUrls: ['./journal-teachers-table.component.scss']
})
export class JournalTeachersTableComponent implements OnInit {
  displayedColumnsTeachers: string[] = ['id', 'firstname', 'lastname', 'middlename', 'group_names', 'group_courses'];
  dataSourceTeachers = new MatTableDataSource(this.journalService.getTeachersJournal());
  constructor(private journalService: JournalService) { }
  @ViewChild(MatPaginator) paginatorTeachers: MatPaginator;
  @ViewChild(MatSort) sortTeachers: MatSort;
  ngOnInit() {
    this.dataSourceTeachers.paginator = this.paginatorTeachers;
    this.dataSourceTeachers.sort = this.sortTeachers;
  }
  alert(row) {
    alert(row.id + ' ' + row.lastname + ' ' + row.firstname + ' ' + row.middlename + ' ' + row.group_names);
  }
}
