import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JournalTeachersTableComponent } from './journal-teachers-table.component';

describe('JournalTeachersTableComponent', () => {
  let component: JournalTeachersTableComponent;
  let fixture: ComponentFixture<JournalTeachersTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JournalTeachersTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JournalTeachersTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
